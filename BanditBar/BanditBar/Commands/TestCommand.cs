﻿using Bandit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BanditBar
{
    public class TestCommand : Command
    {
        protected override void OnExecute()
        {
            Log.Message("TEST COMMAND EXECUTED");
        }
    }
}
