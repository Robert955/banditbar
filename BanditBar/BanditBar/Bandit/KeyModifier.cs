﻿using System;

namespace Bandit
{
    public enum KeyModifier
    {
        None,
        LeftCtrl,
        RightCtrl,
        LeftShift,
        RightShift,
        LeftWin,
        RightWin
    }
}
