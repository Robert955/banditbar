﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace BanditBar
{
    //[ProtoContract]
    [ProtoContract]
    public class QueryDictionary 
    {
        [ProtoMember(1, OverwriteList = true)]
        public SortedDictionary<char, QueryDictionary> Dict { get; set; }
        [ProtoMember(2)]
        public QueryResults Results { get; set; }
            
        public QueryDictionary()
        {
            Dict = new SortedDictionary<char, QueryDictionary>();
            Results = new QueryResults();
        }

    }
}
