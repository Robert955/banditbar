﻿
using ProtoBuf;
using System;
using System.Windows.Media;

namespace BanditBar
{
    [ProtoContract]
    public class QueryResultItem 
    {
        [ProtoMember(1)]
        public string ResultIcon { get; set; }
        [ProtoMember(2)]
        public string ResultName { get; set; }
        [ProtoMember(3)]
        public string ResultDesc { get; set; }
        [ProtoMember(4)]
        public bool IconFromFile { get; set; }

        public ImageSource ResultIconBitmap { get; set; }
        
        public QueryResultItem()
            : this("Images\\TransparantIcon.png", "None", "None")
        {

        }   
             
        public QueryResultItem(string resultIcon, string resultName, string resultDesc)
        {
            ResultIcon = string.Format("{0}\\{1}", Environment.CurrentDirectory, resultIcon);
            ResultName = resultName;
            ResultDesc = resultDesc;
        }

        public QueryResultItem(bool iconFromFile, string resultName, string resultDesc)
            : this("Images\\TransparantIcon.png", resultName, resultDesc)
        {
            this.IconFromFile = iconFromFile;
        }

        public override string ToString()
        {
            return ResultName;
        }

    }
}
